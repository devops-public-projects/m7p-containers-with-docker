
# Deploy a Java Application with Docker Compose on a Remote Server
This project will cover an entire start to finish testing and deployment of a Java application. We will build the code with Gradle, test it with containerized MySQL and phpMyAdmin to verify data is persisted. We'll then containerize our Java app with a Dockerfile and craft a Docker Compose yaml in order to run everything together with ease.  We'll also upload the custom image to a private Nexus Docker Repository. This all simulates what would happen in a build automation tool but all manual!

## Technologies Used
- Java
- Gradle
- Docker
- MySQL
- phpMyAdmin
- Nexus Artifact Repository Manager
- Git
- Linux (Ubuntu)


## Project Description
- Download our Code from a Git repo and create a new repo for testing our changes
- Testing the app with containerized MySQL and phpMyAdmin for data visualization
- Craft a Docker compose file for MySQL and phpMyAdmin for ease of use in testing
- Dockerize our Java application
- Push our Java Docker image to a Nexus private repository
- Add our Java Docker image to our Docker Compose
- Run our application and make sure everything works as Docker containers

## Prerequisites
- Linux Machine with Git, Docker, Java (17 JDK), Gradle installed
- Digital Ocean Account
- Nexus configured and accessible on a remote Droplet
	- A repository named `docker-hosted`
	- A role that grants all permissions (non-admin) for `docker-hosted` repository
	- A user named `docker-user` with the above role as a permission
	- Realm enabled for Docker Bearer Tokens to be granted
	- Port 8090 configured for HTTP for the `docker-hosted` repository

## Guide Steps
### Step 0 | Clone Source Code & Create a new Git Repository
#### Download Files
- `mkdir ~/repos/m7p`
- `cd ~/repos/m7p`
- `git clone https://gitlab.com/twn-devops-bootcamp/latest/07-docker/docker-exercises`
- `cd docker-exercises`
- `rm -rf git*`
- `cd ..`

#### Make new Repository
- From your web browser, create a new Gitlab repository that we can upload our files to. Then initialize the repo with the below commands for that repo.
	- `git init –initial-branch=main`
	- `git remote add origin git@gitlab.com:devops-public-projects/m7p-containers-with-docker.git`
	- `git add .`
	- `git commit -m “Initial Commit”`
	- `git push –set-upstream origin main`

### Step 1 | Start MySQL Container
#### Export Env Variables
The code uses environment variables to get necessary information so we don't hard code credentials into the source code!
- `export DB_USER=admin`
- `export DB_PWD=password`
- `export DB_SERVER=mysql`
- `export DB_NAME=mydb`
- `reload .bashrc`

#### Start MySQL Container
```
docker run --name mysql \
-p 3306:3306
-e MYSQL_ROOT_PASSWORD=root_password \
-e MYSQL_USER=admin \
-e MYSQL_PASSWORD=password \
-e MYSQL_DATABASE=mydb \
-d mysql:8.1.0
```
#### Build and Run Java App
- `cd docker-exercises`
- `build gradle`
	- The JAR file will be in `/build/libs/`

![Build Successful](/images/m7p-initial-build-success.png)

- `java -jar ./build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar`

![App is running successfully](/images/m7p-build-running-with-test-container.png)

#### Test Application
- You can open the application by navigating to `localhost:8080` in your browser. Making changes will be saved in the Database at this point!

![Site Running](/images/m7p-test-site-running-1.png)

![Changes made to site update Database](/images/m7p-test-site-running-2.png)

### Step 2 | Start MySQL GUI Container
#### Start phpMyAdmin Container
- `docker run -d --name phpmyadmin –link mysql:db -p 8081:80 phpmyadmin`

#### Test phpMyAdmin Interface
- Navigate to `localhost:8081` in your web browser
- Login with our configured `admin` and `password` to test it works

![phpMyAdmin is Accessible](/images/m7p-phpmyadmin-accessible.png)

![phpMyAdmin connects to our MySQL database](/images/m7p-phpmyadmin-database-view.png)

### Step 3 | Craft Docker Compose for MySQL and MySQL GUI
Before we continue, stop both the MySQL and phpMyAdmin containers and your running Java application.

#### Making a Docker Compose File
- We created a `docker-compose.yaml` for [MySQL](https://hub.docker.com/_/mysql) and [phpMyAdmin](https://hub.docker.com/_/phpmyadmin) based on their respective Docker Hub pages.
- `docker compose -f docker-compose.yaml up -d`

#### Testing Everything Still Works
- `java -jar ./build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar`
- Access via the web browser your app `localhost:8080` and phpMyAdmin `localhost:8081` .
	- Make changes and verify they are saved accordingly

- `localhost:8080` Java App is available again!

![Java App is Running Successfully with Docker Container MySQL](/images/m7p-java-app-accessible-again.png)


- `localhost:8081` phpMyAdmin is available again!

![phpMyAdmin is running and connected to MySQL](/images/m7p-database-online-again.png)


- Make Changes to Website

![Changes Made to Website](/images/m7p-testing-changes-with-compose-containers.png)


#### Test Data Persists with Volumes
- `docker compose -f docker-compose.yaml down`
- `docker compose -f docker-compose.yaml up -d`
- Connect to your app and the previous changes are still there!

![Changes Persist with Docker Volumes](/images/m7p-changes-saved-compose-containers.png)

### Step 4 | Dockerize the Java App
- We'll use our Dockerfile to place the built application JAR file into a container with the necessary environment variables to allow it to run.
- `cd ~/repos/m7p/docker-exercises`
- `docker build -t java-app:1.0 .`
- `cd ..`
- `docker compose -f docker-compose.yaml up -d`
- `docker run -d -p 8080:8080 -p 8090:8090 –net m7p_default java-app:1.0`
	- Note, the network we use is what Docker creates when we run the `docker-compose.yaml`. If you used a custom network you can change that entry. If you don't have it on the same network the App will not be able to talk to the database!

Make sure to shut everything down before the next section
- `docker compose -f docker-compose.yaml down`
- `docker stop java-app-id`

### Step 5 | Build and Push Java App Docker Image
Note: See the Prerequisites section for how Nexus needs to be configured
- We need to enable insecure registries since we haven't configured HTTPs yet on our Nexus server. 
	- `sudo vim /etc/docker/daemon.json`
		- Add a section like this `{ "insecure-registries" : ["DROPLET_IP:8090"] }`
	- `sudo systemctl restart docker`
	- `docker login DROPLET_IP:8090`
		- user = `docker-user`
	- `docker tag java-app:1.0 droplet_ip:8090/java-app:1.0`
	- `docker push droplet_ip:8090/java-app:1.0`
	- Check the image from CLI (Will print a list of items)
		- `curl -u docker-user:docker_password -X GET ‘http://NEXUS_IP:8090/service/rest/v1/components?repository=docker-hosted`

![Commands to run](/images/m7p-commands-run-for-nexus.png)

![Successful Nexus Upload](/images/m7p-nexus-upload-success.png)

### Step 6 | Add Java App to Docker Compose
We now want to modify our environment variables to not hardcode sensitive information and checking that into our Git repository. We also want our application added to the `docker-compose.yaml` so we can run everything from one single command. We also will benefit from this later when we don't need to rebuild our docker image if we modify environment variables.

To modify the environment variables, we will utilize system environment variables to be copied into the containers when they start up. We can do this by using `MYSQL_DATABASE=${DB_NAME}` for each of them.

We also want to consider a few things, our application will fail to start if it cannot connect to a database. Since Docker cannot guarantee MySQL will finish starting up before our java-app does, we will set our app to `restart: always`. I'll also add a `depends_on: mysql`which will allow MySQL to start running first, but not necessarily finish starting before java-app starts, so we will still need the restart policy in place.

- Test that everything works with the new `docker-compose.yaml`
	- `docker compose -f docker-compose.yaml up -d`

From here I tested and confirmed that everything starts up, the environment variables are set in each container as intended. All three services are accessible and modifications are persistent when turning off and on the containers again.

#### Testing Each Container
- Java-App & phpMyAdmin: Web Browser Accessible
- All Three: 
	- `docker exec -it container_id /bin/bash`
		- `env` shows all necessary environment variables from the system were used
	- `docker logs container_id`
		- Everything is working, no errors, all containers running successfully with their normal output and logging messages

### Step 7 | Run Application on Server with Docker Compose

#### Modify Application Code
Our application is currently configured to utilize localhost as a server, we will modify the line in `index.html` to use our new Droplet Server IP.

We will re-use the version 1.0. We could update it to 1.1 but that would require changes in our `Dockerfile`, and `docker-compose.yaml`. It's easier for right now to use the same version.

- `vim src/main/resources/statis/index.html`
	- Change `const HOST = localhost` to `const HOST = "DROPLET_IP";`

#### Rebuild Java Code
- `gradle build`
	- We will get a new JAR output for 1.0-SNAPSHOT in the `/build/libs` directory

#### Rebuild Docker Image
- First remove the old image and container
	- Container ID: `docker ps -a`
		- `docker rm container_id`
	- Image ID: docker images
		- `docker rmi image_id`
		- Perform for both the `java-app:1.0` and the `DROPLET_IP:8090/java-app:1.0`
- Rebuild our new image
	-  `docker build -t java-app:1.0 .`

#### Push New Code to Private Docker Repository on Nexus
- `docker tag java-app:1.0 NEXUS_IP:8090/java-app:1.0`
- `docker push NEXUS_IP:8090/java-app:1.0`

#### Create New Droplet Server
- `scp configure_app_server root@DROPLET_IP:/root`
- `sudo ./configure_app_server`
	- Installs Docker via Apt
	- Creates a Docker group
	- Creates an `app-runner` account as member of Docker group
- Firewall
	- Allow this server IP to be accessible to our Nexus Server IP in the DigitalOcean portal. If not, `docker login` will fail to our private repository
- Docker Insecure Registries
	- `sudo vim /etc/docker/daemon.json`
	- Add a line for `{ “insecure-registry” : [“NEXUS_IP:8090”] }`

#### Send the Docker Compose YAML to our Server
- `scp docker-compose.yaml app-runner@DROPLET_IP:/app`

#### Configure Env Variables
- `su app-runner`
- Variables Needed
	- `export DB_USER=admin`
	- `export DB_PWD=password`
	- `export DB_ROOT_PASSWORD=root_password`
	- `export DB_NAME=mydb`
	- `export DB_SERVER=mysql`

#### Run Application
- `docker login NEXUS_IP:8090`
	- user: `docker-user`
- `docker volume create –name mysql-data`
- `docker compose -f docker-compose.yaml up -d`

#### Test Application
- Make a modification to the user data
- `docker compose -f docker-compose.yaml down`
- `docker compose -f docker-compose.yaml up -d`
- Verify changes persist
- Check container Environment Variables
	- `docker exec -it container_id /bin/bash`
	- `env | grep DB` or `env | grep MYSQL`
- Check Logs
	- `docker logs container_id`

### Step 8 | Open Firewall Ports
- In your DigitalOcean menu make sure you open port 8080 in order for your website to be accessible!
- Navigate from your local device in the web browser at `DROPLET_IP:8080`

![Everything is running on a remote server!](/images/m7p-finished-project.png)

### Comments/Extras
- [Running Docker as non-root](https://docs.docker.com/engine/security/rootless/)
- [Running Docker as non-root](https://docs.docker.com/engine/security/rootless/)
